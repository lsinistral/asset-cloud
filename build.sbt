val commonSettings = Seq(
  organization := "net.viacheslav",
  scalaVersion := "2.12.4",
  version := "0.0.1"
)


val fs = Seq(
  "com.github.pathikrit" %% "better-files"      % "3.4.0",
  "com.github.pathikrit" %% "better-files-akka" % "3.4.0"
)

val akkaVersion = "2.5.13"

val akka = Seq(
  "com.typesafe.akka" %% "akka-actor"   % akkaVersion,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test,
)

val log = Seq(
  "org.apache.logging.log4j" % "log4j-api" % "2.11.0",
  "org.apache.logging.log4j" % "log4j-core" % "2.11.0",
  "org.apache.logging.log4j" %% "log4j-api-scala" % "11.0"
)

val utils = Seq(
  "com.github.nscala-time" %% "nscala-time" % "2.18.0",
  "com.typesafe"            % "config"      % "1.3.3",
  "com.github.scopt"       %% "scopt"       % "3.7.0"
)

val javax = Seq(
  "javax.xml.bind" % "jaxb-api" % "2.2.8"
)

val play_test = Seq(
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test
)

val test = Seq(
  "org.scalatest" %% "scalatest" % "3.0.5" % "test"
)
val db = Seq(
  "com.typesafe.slick" %% "slick" % "3.2.3",
  "org.apache.logging.log4j" % "log4j-slf4j-impl" % "2.11.0",
  "com.typesafe.slick" %% "slick-hikaricp" % "3.2.3",
  "org.postgresql" % "postgresql" % "42.2.2"
)

lazy val `mediamanager-core` = (project in file("core"))
  .settings(commonSettings: _*)
  .settings(
    name := """mediamanager-core""",
    libraryDependencies ++= test ++ utils ++ db ++ log
  )
lazy val `mediamanager-storage` = (project in file("storage"))
  .settings(commonSettings: _*)
  .settings(
    name := """mediamanager-storage""",
    libraryDependencies ++= fs ++ log ++ test ++ utils ++ javax ++ db,
    mainClass in assembly := Some("net.viach.mm.storage.StorageApp"),
  ).dependsOn(`mediamanager-core` % "test->test;compile->compile")


lazy val `mediamanager_web` = (project in file("web"))
  .enablePlugins(PlayScala)
  .settings(commonSettings: _*)
  .settings(
    name := """mediamanager_web""",
    libraryDependencies ++= Seq(jdbc, ehcache, ws, specs2 % Test, guice),
    libraryDependencies ++= play_test ++ javax
  )

lazy val `mediamanager-root` = (project in file("."))
  .settings(commonSettings: _*)
  .settings(
    name := """mediamanager-root""",
  ).aggregate(`mediamanager_web`, `mediamanager-storage`, `mediamanager-core`)



// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.example.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.example.binders._"
