package net.viacheslav.mm.core

import slick.jdbc.PostgresProfile.api._
import slick.sql.SqlProfile.ColumnOption.SqlType


class FilesInStorageTbl(tag: Tag) extends Table[StoragedFile](tag, "files_in_storages") {

  def id = column[Long]("id", O.PrimaryKey, O.Unique, O.AutoInc)

  def filename = column[String]("filename", SqlType("TEXT"))

  def subpath = column[String]("subpath", SqlType("TEXT"))

  def size = column[Long]("size")

  def hash = column[String]("hash", O.Length(40, varying = false))

  def storage_id = column[Int]("storage_id")

  def storage =
    foreignKey("storage", storage_id, StoragesTbl.q)(_.id, onUpdate = ForeignKeyAction.Cascade)

  def local_singularity =
    index("local_singularity", (filename, subpath, storage_id), unique = true)

  def * = (id, filename, subpath, size, hash, storage_id) <> ((StoragedFile.db_apply _).tupled, StoragedFile.db_unapply)
}

object FilesInStorageTbl {
  val q = TableQuery[FilesInStorageTbl]
}
