package net.viacheslav.mm.core

import slick.jdbc.PostgresProfile.api._

object ManageDatabase {
  val db = Database.forConfig("db")

  private val createRelationsQuery =
    StoragesTbl.q.schema.create andThen
      FilesInStorageTbl.q.schema.create

  private val deleteRelationsQuery =
    FilesInStorageTbl.q.schema.drop andThen
      StoragesTbl.q.schema.drop

  def createRelations() =
    db.run(createRelationsQuery)

  def deleteRelations() =
    db.run(deleteRelationsQuery)

}

