package net.viacheslav.mm.core

import org.apache.logging.log4j.scala.Logging
import org.postgresql.util.PSQLException

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

object ManagementApp extends App with Logging {
  private[this] var createDatabase = false
  private[this] var deleteDatabase = false


  args.foreach {
    case "--db-recreate" => deleteDatabase = true
                            createDatabase = true
    case "--db-delete"   => deleteDatabase = true
    case "--db-create"   => createDatabase = true
  }

  if (deleteDatabase) {
    Await.result(ManageDatabase.deleteRelations().recover {
      case e: PSQLException if e.getSQLState == "42P01" =>
        logger.info("No database exist")
      case e: PSQLException =>
        logger.error(s"error code: ${e.getSQLState}, message:${e.getServerErrorMessage}")
    },
      20.seconds)
  }
  if (createDatabase) {
    Await.result(
      ManageDatabase.createRelations().recover {
        case e: PSQLException if e.getSQLState == "42P07" =>
          logger.info("Database already exist")
        case e: PSQLException =>
          logger.error(s"error code: ${e.getSQLState}, message:${e.getServerErrorMessage}")
      },
      20.seconds)
  }
}
