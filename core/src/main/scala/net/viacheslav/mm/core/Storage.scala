package net.viacheslav.mm.core

/**
  * Storage representation
  *
  * @param id Storage ID, taken from the configuration.
  *           If conflict with the system occurs - ID retrieved from the system considered as truthful.
  *           Used as short reference of the storage
  *
  * @param name Storage name, taken from the configuration.
  *             Should be unique within the system. System identify storage by its name, weirdly.
  *
  * @param systemPath Path at which storage is represented in the system. Means raw storage representation.
  *                   May be deprecated in the future.
  *
  * @param localPath Local path to the data represented by storage daemon
  */
case class Storage(id: Int, name: String, systemPath: String, localPath: String)


//You may not need ID, as separate field, as name already identifies storage.
//Also, you may not need system path, as in the system real directory structure will be shadowed.
//System path may be useful for internal tracking of storage changes.