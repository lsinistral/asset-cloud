package net.viacheslav.mm.core

import java.nio.file.{Path, Paths}

case class StoragedFile(filename: String, subPath: Path, size: Long, hash: String, storage: Int, id: Long)

//TODO: Move to table representation
object StoragedFile {
  def db_apply(id: Long, filename: String, subPath: String, size: Long, hash: String, storage: Int): StoragedFile =
    StoragedFile(filename, Paths.get(subPath), size, hash, storage, id)

  def db_unapply(arg: StoragedFile): Option[(Long, String, String, Long, String, Int)] =
    Some((arg.id, arg.filename, arg.subPath.toString, arg.size, arg.hash, arg.storage))
}