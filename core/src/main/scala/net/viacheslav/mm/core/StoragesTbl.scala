package net.viacheslav.mm.core

import slick.jdbc.GetResult
import slick.jdbc.PostgresProfile.api._
import slick.sql.SqlProfile.ColumnOption.SqlType

class StoragesTbl(tag: Tag) extends Table[Storage](tag, "storages") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

  def name = column[String]("name", SqlType("TEXT"), O.Unique)

  def system_path = column[String]("system_path", SqlType("TEXT"), O.Unique)

  def local_path = column[String]("local_path")

  def * = (id, name, system_path, local_path) <> (Storage.tupled, Storage.unapply)
}

object StoragesTbl {
  val q = TableQuery[StoragesTbl]
  implicit val getStorage: GetResult[Storage] = GetResult(r => Storage(r.<<, r.<<, r.<<, r.<<))
}
