package net.viacheslav.mm.test.core

import java.nio.file.Paths

import net.viacheslav.mm.core.{FilesInStorageTbl, Storage, StoragedFile, StoragesTbl}
import net.viacheslav.mm.test.core.helpers.{DatabaseTestSpec, syncQuery}
import org.postgresql.util.PSQLException
import org.scalatest.{FlatSpec, Matchers, Outcome}
import slick.jdbc.PostgresProfile.api._

class DatabaseTest extends DatabaseTestSpec {

  "A postgresql database" should "create storages table" in {
    val q = StoragesTbl.q.schema.create
    val res =
      """create table "storages" (
        |"id" SERIAL NOT NULL PRIMARY KEY,
        |"name" TEXT NOT NULL UNIQUE,
        |"system_path" TEXT NOT NULL UNIQUE,
        |"local_path" VARCHAR NOT NULL)""".stripMargin.replace("\n", "")
    q.statements.mkString(" ") shouldBe res
  }
  it should "create files_in_storages table" in {
    val q = FilesInStorageTbl.q.schema.create
    val res =
      """create table "files_in_storages" (
        |"id" BIGSERIAL NOT NULL PRIMARY KEY UNIQUE,
        |"filename" TEXT NOT NULL,
        |"subpath" TEXT NOT NULL,
        |"size" BIGINT NOT NULL,
        |"hash" CHAR(40) NOT NULL,
        |"storage_id" INTEGER NOT NULL)
        | create unique index "local_singularity" on "files_in_storages" ("filename","subpath","storage_id")
        | alter table "files_in_storages"
        | add constraint "storage" foreign key("storage_id") references "storages"("id")
        | on update CASCADE on delete NO ACTION""".stripMargin.replace("\n", "")
    q.statements.mkString(" ") shouldBe res
  }

  it should "add new storage to the table" in {
    val storage               = Storage(1, "Test storage",        "/path/to/storage",             "/local/path")
    val sameIdStorage         = Storage(1, "Different storage",   "/path/to/different/storage",   "/diff/local/path")
    val differentStorage      = Storage(2, "Another storage",     "/path/to/another/storage",     "/diff/local/path")
    val sameLocalPathStorage  = Storage(3, "Yet another storage", "/path/to/yet/another/storage", "/local/path")

    syncQuery(StoragesTbl.q += storage)
    syncQuery(StoragesTbl.q += differentStorage)
    syncQuery(StoragesTbl.q += sameLocalPathStorage)
    syncQuery(StoragesTbl.q += sameIdStorage) //system ignores given id and assigns its own

    val result = syncQuery(StoragesTbl.q.result)

    result.length shouldBe 4
  }
  it should "not add storage with duplicate system path or name" in {
    val storage               = Storage(1, "Test storage",      "/path/to/storage",           "/local/path")
    val sameNameStorage       = Storage(2, "Test storage",      "/path/to/different/storage", "/different/local/path")
    val sameSystemPathStorage = Storage(3, "Different storage", "/path/to/storage",           "/different/local/path")

    syncQuery(StoragesTbl.q += storage)

    val ex1 = the[PSQLException] thrownBy syncQuery(StoragesTbl.q += sameNameStorage)
    ex1.getServerErrorMessage.getConstraint shouldBe "storages_name_key"

    val ex2 = the[PSQLException] thrownBy syncQuery(StoragesTbl.q += sameSystemPathStorage)
    ex2.getServerErrorMessage.getConstraint shouldBe "storages_system_path_key"

    val result = syncQuery(StoragesTbl.q.result)
    result.length shouldBe 1
    result.head shouldBe storage
  }

  it should "add new file to the table" in {
    val hash  = "0000000000000000000000000000000000000000"
    val hash2 = "0000000000000000000000000000000000000001"
    val hash3 = "0000000000000000000000000000000000000002"
    val hash4 = "0000000000000000000000000000000000000003"
    val storage   = Storage(1, "Test storage",    "/path/to/storage", "/local/path")
    val storage2  = Storage(2, "Another storage", "/another/storage", "/local/path")
    syncQuery(StoragesTbl.q += storage)
    syncQuery(StoragesTbl.q += storage2)

    //IDs are ignored by the database
    val file                      = StoragedFile("file.txt",  Paths.get("test/directory"),    1024, hash,   1, 1)
    val differentPathFile         = StoragedFile("file.txt",  Paths.get("another/directory"), 1024, hash,   1, 2)
    val differentPathAndHashFile  = StoragedFile("file.txt",  Paths.get("diff/directory"),    4096, hash3,  1, 3)
    val differentNameFile         = StoragedFile("file2.txt", Paths.get("test/directory"),    1024, hash,   1, 4)
    val differentNameAndHashFile  = StoragedFile("file3.txt", Paths.get("test/directory"),    8192, hash4,  1, 5)
    val differentNameAndPathFile  = StoragedFile("file4.txt", Paths.get("another/dir"),       8192, hash4,  1, 6)
    val sameFileDifferentStorage  = StoragedFile("file.txt",  Paths.get("test/directory"),    1024, hash,   2, 7)

    val result  = syncQuery(FilesInStorageTbl.q += file)
    val result2 = syncQuery(FilesInStorageTbl.q += differentPathFile)
    val result3 = syncQuery(FilesInStorageTbl.q += differentPathAndHashFile)
    val result4 = syncQuery(FilesInStorageTbl.q += differentNameFile)
    val result5 = syncQuery(FilesInStorageTbl.q += differentNameAndHashFile)
    val result6 = syncQuery(FilesInStorageTbl.q += differentNameAndPathFile)
    val result7 = syncQuery(FilesInStorageTbl.q += sameFileDifferentStorage)

    result  shouldBe 1
    result2 shouldBe 1
    result3 shouldBe 1
    result4 shouldBe 1
    result5 shouldBe 1
    result6 shouldBe 1
    result7 shouldBe 1


    val data = syncQuery(FilesInStorageTbl.q.result)

    data.length shouldBe 7
    data.toList should contain only(file, differentPathFile, differentPathAndHashFile, differentNameFile,
      differentNameAndHashFile, differentNameAndPathFile, sameFileDifferentStorage)

  }
  it should "not add new file to the table" in {
    val sha   = "0000000000000000000000000000000000000000"
    val hash2 = "0000000000000000000000000000000000000001"
    val storage = Storage(1, "Test storage", "/path/to/storage", "/local/path")
    val file              = StoragedFile("filename.txt", Paths.get("test/directory"), 1024, sha,   1, 0)
    val sameFile          = StoragedFile("filename.txt", Paths.get("test/directory"), 1024, sha,   1, 0)
    val differentHashFile = StoragedFile("filename.txt", Paths.get("test/directory"), 2048, hash2, 1, 1)
    syncQuery(StoragesTbl.q += storage)
    syncQuery(FilesInStorageTbl.q += file)

    val result  = the[PSQLException] thrownBy syncQuery(FilesInStorageTbl.q += sameFile)
    val result2 = the[PSQLException] thrownBy syncQuery(FilesInStorageTbl.q += differentHashFile)

    result.getSQLState shouldBe "23505"
    result.getServerErrorMessage.getConstraint shouldBe "local_singularity"
    result2.getSQLState shouldBe "23505"
    result2.getServerErrorMessage.getConstraint shouldBe "local_singularity"

  }
}