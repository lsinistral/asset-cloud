package net.viacheslav.mm.test.core.helpers

import net.viacheslav.mm.core.{FilesInStorageTbl, StoragesTbl}
import org.scalatest.Outcome
import slick.jdbc.PostgresProfile.api._

trait DatabaseTestSpec extends TestSpec {

  override def withFixture(test: NoArgTest): Outcome = {
    syncQuery(StoragesTbl.q.schema.create)
    syncQuery(FilesInStorageTbl.q.schema.create)
    try super.withFixture(test) // Invoke the test function
    finally {
      syncQuery(FilesInStorageTbl.q.schema.drop)
      syncQuery(StoragesTbl.q.schema.drop)

    }
  }

}
