package net.viacheslav.mm.test.core

import scala.concurrent.{Await, Future}
//import scala.concurrent.ExecutionContext.Implicits.global

package object helpers {


  import slick.jdbc.PostgresProfile.api._

  import scala.concurrent.duration._

  val db = Database.forConfig("db")

  /**
    * Runs given future
    *
    * @param future Future to run
    * @tparam T Return type
    * @return Result of the future
    */
  def runFuture[T](future: Future[T]): T = Await.result(future, 20.seconds)

  /**
    * Run slick queries synchronously with maximum wait time of 20 seconds
    *
    * @param query Query to run
    * @tparam T Returning type
    * @return Result of the query
    */
  def syncQuery[T](query: DBIOAction[T, NoStream, Nothing]): T = runFuture(db.run(query))
}
