package net.viach.mm.storage

import net.viach.mm.storage.configuration.Configurator

object AppConfig {
  val storage = new Configurator()
}
