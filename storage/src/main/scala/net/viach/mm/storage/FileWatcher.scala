package net.viach.mm.storage

import java.nio.file.Path

import better.files.{File, FileMonitor}
import net.viach.mm.storage.processing.ProcessDirectory
import org.apache.logging.log4j.scala.Logger

object FileWatcher {


  private[this] val logger = Logger(getClass)
  private[this] val root = File(StorageSettings.baseDirectory)

  def initialize(): Unit = {
    import scala.concurrent.ExecutionContext.Implicits.global

    watcher.start()
  }


  private[this] val watcher = new FileMonitor(root) {

    override def onCreate(file: File, count: Int): Unit = {
      if (file.isDirectory) ProcessDirectory(file.path.toString, recursive = false)
      logger.debug(prepareMessage("added", file))
    }

    override def onModify(file: File, count: Int): Unit = {
      if (file.isDirectory) ProcessDirectory(file.path.toString, recursive = false)
      logger.debug(prepareMessage("modified", file))
    }

    override def onDelete(file: File, count: Int): Unit = {
      logger.debug(prepareMessage("deleted", file))
    }


  }

  def prepareMessage(action: String, file: File, subpath: Path = File("/").path): String = {
    s"$action: ${root.path.relativize(file.path)}"
  }
}
