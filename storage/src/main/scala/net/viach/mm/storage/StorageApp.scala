package net.viach.mm.storage


import net.viach.mm.storage.processing.ProcessDirectory
import org.apache.logging.log4j.scala.Logging

import scala.util._

object StorageApp extends App with Logging {

  case class Conf(dir: String, withWatcher: Boolean, withConfiguration: Boolean, withScan: Boolean)
  object Conf {
    val default: Conf = Conf(
      AppConfig.storage.getStorage.localPath,
      withWatcher = true,
      withConfiguration = true,
      withScan = true)
  }

  val parser = new scopt.OptionParser[Conf]("mm-storage") {
    head("Mediamanager storage daemon", "0.1")

    opt[String]('d', "dir")
      .action((p, c) => c.copy(dir = p))
      .text("Directory which will be treated as parent")

    opt[Unit]("no-scan")
      .action((_, c) => c.copy(withScan = false))
      .text("No directory scan will be performed")

    opt[Unit]("no-config")
      .action((_, c) => c.copy(withConfiguration = false))
      .text("No storage configureation will be performed")

    opt[Unit]("no-watcher")
      .action((_, c) => c.copy(withWatcher = false))
      .text("Daemon will not watch for changes in directories")
  }

  parser.parse(args, Conf.default) match {
    case Some(c) =>
      if (c.withConfiguration) {
        logger.info("Starting storage configuration")
        AppConfig.storage.configureStorage match {
          case Success(_) =>
            logger.info("Storage configured")
          case Failure(e) => logger.error(e)
        }
      }

      if (c.withScan) {
        logger.info("Starting storage scan")
        ProcessDirectory(c.dir)
        logger.info("Storage scan finished")
      }

      if (c.withWatcher) {
        logger.info("Starting storage file watcher")
        FileWatcher.initialize()
        scala.io.StdIn.readLine()
      }

      scala.io.StdIn.readLine()

    case None => logger.error("Command arguments are wrong")
  }
}
