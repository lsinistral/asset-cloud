package net.viach.mm.storage

import com.typesafe.config.{Config, ConfigFactory, ConfigValueFactory}

import scala.util.matching.Regex

object StorageSettings {

  import scala.collection.JavaConverters._

  private[this] val default = ConfigFactory.empty()
    .withValue("mm.storage.systemDir", ConfigValueFactory.fromAnyRef(""))
    .withValue("mm.storage.id", ConfigValueFactory.fromAnyRef(-1))

  private[this] val conf: Config = ConfigFactory.load().withFallback(default)


  val storageId: Int = conf.getInt("mm.storage.id")

  /** Current:
    * storage name, system will identify the storage by that name
    *
    * Possible alteration:
    * preferable storage name
    * actual name is defined by the system
    * if system already has a storage with same id and different name, the one in the system will be used
    * to get there we need to introduce ID of the storage and whole configuration procedure */
  val storageName: String = conf.getString("mm.storage.name")

  val systemDirectory: String = conf.getString("mm.storage.systemDir")


  val baseDirectory: String = conf.getString("mm.storage.baseDir")
  val filesToIgnore: List[Regex] =
    conf.getStringList("mm.storage.ignore").asScala.toList.map(new Regex(_))


}
