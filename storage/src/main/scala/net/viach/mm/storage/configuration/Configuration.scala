package net.viach.mm.storage.configuration

import net.viacheslav.mm.core.Storage

import scala.concurrent.Future
import scala.util._

trait Configuration {
  def checkStorage(name: String): Future[Option[Storage]]

  def addStorage(st: Storage): Future[Try[Int]]

  def updateConfig(storage: Storage): Try[Storage]

  def getStorage: Storage

  def configureStorage: Try[Storage]
}

object Configuration {

  object Exceptions {

    case class DuplicateNameException(name: String) extends Exception(s"Storage with name $name already exists")

    case class DuplicatePathException(systemPath: String) extends Exception(s"Storage with system path $systemPath already exists")

  }

}

