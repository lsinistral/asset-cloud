package net.viach.mm.storage.configuration

import better.files.File
import net.viach.mm.storage.StorageSettings
import net.viach.mm.storage.configuration.Configuration.Exceptions.{DuplicateNameException, DuplicatePathException}
import net.viacheslav.mm.core.{Storage, StoragesTbl}
import org.apache.logging.log4j.scala.Logging
import org.postgresql.util.PSQLException
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future, TimeoutException}
import scala.util.{Failure, Success, Try}

class Configurator() extends Configuration with Logging {


  private[this] var currentStorage =
    Storage(StorageSettings.storageId,
      StorageSettings.storageName,
      StorageSettings.systemDirectory,
      StorageSettings.baseDirectory)

  val db = Database.forConfig("db")

  /**
    * Checks if storage with given `name` can be found in the system
    *
    * @param name storage name to check
    * @return [[Storage]] if found, [[None]] if nothing found
    */
  def checkStorage(name: String): Future[Option[Storage]] = {
    db.run(StoragesTbl.q.filter(_.name === name).result.head).map(Some(_)).recoverWith {
      case _: Throwable =>
        Future {
          None
        }
    }
  }

  def checkStorage(): Future[Option[Storage]] = checkStorage(StorageSettings.storageName)

  def addStorage(st: Storage): Future[Try[Int]] = {

    db.run((StoragesTbl.q returning StoragesTbl.q.map(_.id)) += st)
      .map(Success(_))
      .recover {
        case ex: PSQLException if
        ex.getSQLState == "23505" && ex.getServerErrorMessage.getConstraint ==
          "storages_name_key" =>
          Failure(DuplicateNameException(st.name))

        case ex: PSQLException if
        ex.getSQLState == "23505" && ex.getServerErrorMessage.getConstraint ==
          "storages_system_path_key" =>
          Failure(DuplicatePathException(st.systemPath))

        case ex: PSQLException => Failure(ex)

        case _ => Failure(new Exception("Something went really wrong!"))
      }
  }


  /**
    * Updates storage configuration.
    * If base directory changed - updates system,
    * if any other - updates config
    *
    * @param storage Storage information from the system
    * @return configured storage or error
    */
  def updateConfig(storage: Storage): Try[Storage] = {
    import scala.concurrent.duration._
    val q = for {
      st <- StoragesTbl.q if st.name === currentStorage.name
    } yield st.local_path
    val future = db.run(q.update(currentStorage.localPath))

    try {
      Await.result(future, 20.seconds)
      currentStorage = storage.copy(localPath = currentStorage.localPath)
      logger.debug("storage configuration updated")
      Success(getStorage)
    } catch {
      case _: TimeoutException => Failure(new Exception("Timeout updating storage local path"))
      case _: Exception => Failure(new Exception("Error updating storage local path"))
    }

  }

  /**
    *
    * @return configured storage
    */
  def getStorage: Storage = {
    currentStorage
  }


  private def checkIfDirectoryExist[T](block: => Try[T]): Try[T] = {
    val file = File(currentStorage.localPath)
    if (file.exists && file.isDirectory) {
      block
    }
    else {
      Failure(new Exception(s"${currentStorage.localPath} must exist and be directory"))
    }
  }

  /**
    * Configuration routine for scenario where no storage found in the system
    *
    * @return
    */
  private[this] def noStorageConfig(): Try[Int] = {
    import scala.concurrent.duration._
    logger.info(s"No storage with `${currentStorage.name}` name in the system, configuring")

    checkIfDirectoryExist {
      val future = this.addStorage(this.getStorage)
      try {
        Await.result(future, 20.seconds)
      } catch {
        case _: TimeoutException => Failure(new Exception("Timeout adding new storage"))
        case _: Throwable => Failure(new Exception("Error adding new storage to the system"))
      }
    }
  }

  /**
    * Configuration routine for scenario where storage with given name exists
    *
    * @param s Storage information from the system
    */
  private[this] def storageExistConfig(s: Storage): Try[Storage] = {
    logger.info(s"Found storage with `${currentStorage.name}` name, checking for changes")
    val st = s
    checkIfDirectoryExist {
      if (!(currentStorage equals st)) {
        if (!(currentStorage.copy(id = st.id) equals st)) {
          logger.info("path changes found, updating storage config...")
        }
        else {
          logger.info("storage id changed")
        }
        updateConfig(st)
      }
      else {
        logger.info("no changes found")
        Success(st)
      }
    }
  }


  /**
    * Storage configuration routine
    *
    * Checks if there is a storage with given(in the config) name in the system,
    * if there is - checks for changes in paths
    *
    * if there is no storage - adds storage to the system
    *
    * @return Configured storage instance
    */
  def configureStorage: Try[Storage] = {
    import scala.concurrent.duration._
    var configured = false
    var error: Throwable = new Exception("Something wrong")

    val conf = this.checkStorage().map {
      case None =>
        noStorageConfig() match {
          case Success(i) =>
            currentStorage = currentStorage.copy(id = i)
            configured = true
          case Failure(x: Throwable) => error = x
        }

      case Some(s) =>
        storageExistConfig(s) match {
          case Success(_) => configured = true
          case Failure(x: Throwable) => error = x
        }
    }

    Await.result(conf, 20.seconds)
    if (configured) Success(currentStorage)
    else Failure(error)
  }
}
