package net.viach.mm.storage.processing

import net.viach.mm.storage.StorageSettings

trait IgnoredFiles {
  /**
    * Check if file is ignored based on configuration file
    *
    * @param filename Filename to check
    * @return true if filename is in the ignored list, false otherwise
    */
  def isFileIgnored(filename: String): Boolean =
    StorageSettings.filesToIgnore
      .map(r => r.findFirstMatchIn(filename))
      .exists(r => r.isDefined)

}
