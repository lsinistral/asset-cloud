package net.viach.mm.storage.processing

import better.files.File
import net.viach.mm.storage.processing.ProcessFile.{ActionError, FileActionResult, FileAdded, FileHashUpdated}
import net.viacheslav.mm.core.{FilesInStorageTbl, StoragedFile}
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Future
import scala.util.control.NonFatal

trait PostgreSQLFileProcess extends ProcessFile {
  val db = Database.forConfig("db")

  /**
    * Check if there is identical(Same name, path, storage) file in the system
    *
    * @param file File to check
    */
  def checkForIdentical(file: File): Future[Option[StoragedFile]] = {
    val storedFile = prepareStoredFile(file)
    val q = FilesInStorageTbl.q.filter(
      f => f.filename   === storedFile.filename &&
           f.subpath    === storedFile.subPath.toString &&
           f.storage_id === storedFile.storage)

    db.run(q.result).map(_.headOption)
  }

  /**
    * Adds new file to the system. If no file with the same hash were found.
    *
    * @param file File to add
    * @return [[net.viach.mm.storage.processing.ProcessFile.FileAdded]] if file was successfully added,
    *         [[net.viach.mm.storage.processing.ProcessFile.ActionError]] if the was an error
    */
  def addNewFile(file: File): Future[FileActionResult] = {
    val q = FilesInStorageTbl.q returning FilesInStorageTbl.q.map(_.id) into ((item, id) => item.copy(id = id))
    db.run(q += prepareStoredFile(file))
      .map(FileAdded)
      .recover {
        case NonFatal(e) => ActionError(e.getMessage)
      }
  }

  /**
    * Updates hash in existent file, when changed
    *
    * @param file    File to update hash for. With old hash in it
    * @return [[Future]] with
    *         [[net.viach.mm.storage.processing.ProcessFile.FileHashUpdated]] if hash was updated successfully
    *         [[net.viach.mm.storage.processing.ProcessFile.ActionError]] if the was an error
    */
  def updateHash(file: StoragedFile): Future[FileActionResult] = {
    val q = (for {f <- FilesInStorageTbl.q if f.id === file.id} yield f.hash).update(file.hash)

    db.run(q).map(_ => FileHashUpdated(file)).recover {
      case NonFatal(e) => ActionError(e.getMessage)
    }
  }
}