package net.viach.mm.storage.processing

import java.nio.file.{Path, Paths}

import better.files.File
import net.viach.mm.storage.StorageSettings
import net.viach.mm.storage.processing.ProcessFile._
import org.apache.logging.log4j.scala.Logging

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

trait ProcessDirectory extends Logging {
  val processor: ProcessFile

  def apply(baseDir: String, recursive: Boolean): Unit = {
    logger.info("File addition procedure started")
    processDir(File(baseDir), recursive)
    logger.info("File addition finished")
  }

  def apply(baseDir: String): Unit = apply(baseDir, recursive = true)

  def apply(): Unit = apply(StorageSettings.baseDirectory)

  //TODO: Make processing tail recursive or stream based
  def processDir(dir: File, recursive: Boolean): Unit = {
    logger.info(s"processing directory: $dir")
    val (dirs, files) = dir.children.partition(_.isDirectory)
    files.toList.foreach(processFile)
    if (recursive) dirs.toList.foreach(d => processDir(d, recursive = true))
  }

  def processFile(file: File): Unit = {
    import scala.concurrent.ExecutionContext.Implicits.global
    processor.processFile(file).onComplete {
      case Success(r: FileAdded)       => logger.debug(s"New file added. File name: ${r.file.filename}")
      case Success(r: FileNotChanged)  => logger.debug(s"File did not change. File name: ${r.file.filename}")
      case Success(r: FileHashUpdated) => logger.debug(s"File hash updated. File name: ${r.file.filename}")
      case Success(r: FileIgnored)     => logger.debug(s"File ignored. File name: ${r.file.name}")
      case Success(a: ActionError)     => logger.error(s"Error: ${a.message}")
      case Failure(e)                  => logger.error(e.getMessage)
    }

  }


}

object ProcessDirectory extends ProcessDirectory {
  object processor extends PostgreSQLFileProcess {
    implicit val ec: ExecutionContext = ExecutionContext.global
  }

}