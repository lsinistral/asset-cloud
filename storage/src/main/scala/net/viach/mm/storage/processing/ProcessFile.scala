package net.viach.mm.storage.processing

import java.nio.file.{Path, Paths}

import better.files.File
import net.viach.mm.storage.AppConfig
import net.viach.mm.storage.processing.ProcessFile.{FileActionResult, FileIgnored, FileNotChanged}
import net.viacheslav.mm.core.StoragedFile

import scala.concurrent.{ExecutionContext, Future}

trait ProcessFile extends IgnoredFiles {
  implicit val ec: ExecutionContext

  /**
    * Check if there is identical(Same name, path, storage) file in the system
    *
    * @param file File to check
    */
  def checkForIdentical(file: File): Future[Option[StoragedFile]]

  /**
    * Adds new file to the system. If no file with the same hash were found.
    *
    * @param file File to add
    * @return [[Future]] with
    *         [[net.viach.mm.storage.processing.ProcessFile.FileAdded]] if file was successfully added,
    *         [[net.viach.mm.storage.processing.ProcessFile.ActionError]] if the was an error
    */
  def addNewFile(file: File): Future[FileActionResult]

  /**
    * Updates hash in existent file, when changed
    *
    * @param file File to update hash for. With old hash in it
    * @return [[Future]] with
    *         [[net.viach.mm.storage.processing.ProcessFile.FileHashUpdated]] if hash was updated successfully
    *         [[net.viach.mm.storage.processing.ProcessFile.ActionError]] if the was an error
    */
  def updateHash(file: StoragedFile): Future[FileActionResult]

  /**
    * Compute hash to compare file content.
    *
    * @param file File to compute hash for
    * @return String representation of computed hash
    */
  def computeHash(file: File): String = file.sha1

  /**
    * Helper which produces [[StoragedFile]] out of [[File]]
    *
    * @param file [[File]] to transform
    * @return Produced [[StoragedFile]]
    */
  def prepareStoredFile(file: File): StoragedFile = {
    val storage = AppConfig.storage.getStorage

    val path: Path =
      Option(Paths.get(storage.localPath).relativize(file.path).getParent).getOrElse(Paths.get(""))

    // TODO: Somehow remove potentially dangerous storage id dependency. May be misconfigured.
    StoragedFile(file.name, path, file.size, computeHash(file), storage.id, -1)
  }

  /**
    * Actual file processing
    *
    * @param file [[File]] to process
    * @return Future
    */
  protected def process(file: File): Future[FileActionResult] =
    checkForIdentical(file).map {
      case None => addNewFile(file)
      case Some(sf) =>
        val newHash = computeHash(file)
        if (sf.hash != newHash) updateHash(sf.copy(hash = newHash)) else Future {
          FileNotChanged(sf)
        }
    }.flatten

  /**
    * Processes given file based on identity(filename, subpath, storage)
    * If there is identical file - checks if hashes are equal.
    *   Hashes are not equal - updates one in the system
    *   Hashes are equal - does nothing
    * If there no identical file - adds given file to the system
    *
    * @param file [[File]] to process
    * @return [[Future]] with result of processing
    */
  def processFile(file: File): Future[FileActionResult] =
    if (!isFileIgnored(file.name)) process(file) else Future { FileIgnored(file) }

}


object ProcessFile {

  sealed trait FileActionResult

  case class FileAdded(file: StoragedFile) extends FileActionResult

  case class FileHashUpdated(file: StoragedFile) extends FileActionResult

  case class FileNotChanged(file: StoragedFile) extends FileActionResult

  case class FileIgnored(file: File) extends FileActionResult

  case class ActionError(message: String) extends Exception(message) with FileActionResult

}