package net.viacheslav.mm.test.storage

import better.files.File
import net.viach.mm.storage.AppConfig

trait WithFiles {
  def withFile(testCode: Seq[File] => Any): Unit = {
    val testStorage = AppConfig.storage.getStorage
    val dir = File(testStorage.localPath)

    val file = dir.createChild("sub/path/file.txt", createParents = true)
    val file2 = dir.createChild("sub/path/file2.txt", createParents = true)
    val file3 = dir.createChild("another/path/file.txt", createParents = true)
    val file4 = dir.createChild("yet/another/path/file3.txt", createParents = true)
    val file5 = dir.createChild("file4.txt", createParents = true)
    val file6 = dir.createChild("sub/path/file5.txt", createParents = true)
    val file7 = dir.createChild("sub/path/file6.txt", createParents = true)
    val file8 = dir.createChild("path/file.txt", createParents = true)

    file.append("First test file")
    file2.append("Second test file")
    file3.append("Third test file")
    file4.append("Forth test file")
    file5.append("Fifth test file")
    file6.append("First test file")
    file7.append("First test file")
    file8.append("First test file")

    val files: Seq[File] = Seq(file, file2, file3, file4, file5, file6, file7, file8)
    try {
      testCode(files)
    }
    finally dir.clear()
  }
}