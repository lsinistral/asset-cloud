package net.viacheslav.mm.test.storage.core

import net.viach.mm.storage.processing.IgnoredFiles
import net.viacheslav.mm.test.core.helpers.TestSpec

class IgnoredFilesTest extends TestSpec {

  object instance extends IgnoredFiles

  "IgnoredFiles" should "ignore files based on configuration file" in {
    // configuration file:
    // ignore = ["^[.]{1}","ZbThumbnail.info", "\\.txt$"]

    instance.isFileIgnored("picture.jpg")      shouldBe false
    instance.isFileIgnored("DS_Store")         shouldBe false
    instance.isFileIgnored(".DS_Store")        shouldBe true
    instance.isFileIgnored("text.txt")         shouldBe true
    instance.isFileIgnored(".txt")             shouldBe true
    instance.isFileIgnored("ZbThumbnail.info") shouldBe true
  }

}
