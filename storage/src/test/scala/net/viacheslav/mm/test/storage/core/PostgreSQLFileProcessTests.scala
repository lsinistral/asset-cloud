package net.viacheslav.mm.test.storage.core

import net.viach.mm.storage.AppConfig
import net.viach.mm.storage.processing.PostgreSQLFileProcess
import net.viach.mm.storage.processing.ProcessFile.{FileAdded, FileHashUpdated}
import net.viacheslav.mm.core.FilesInStorageTbl
import net.viacheslav.mm.test.core.helpers.{DatabaseTestSpec, runFuture, syncQuery}
import net.viacheslav.mm.test.storage.WithFiles
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext}

class PostgreSQLFileProcessTests extends DatabaseTestSpec with WithFiles {

  object instance extends PostgreSQLFileProcess {
    implicit val ec: ExecutionContext = scala.concurrent.ExecutionContext.global
  }

  "checkForIdentical" should "return None if no identical file found" in withFile { files =>
    def check() = Await.result(instance.checkForIdentical(files.head), 20.seconds)

    def addFile(idx: Int) = syncQuery(FilesInStorageTbl.q += instance.prepareStoredFile(files(idx)))

    AppConfig.storage.configureStorage

    check() shouldBe None

    addFile(1)
    check() shouldBe None

    addFile(2)
    check() shouldBe None

    addFile(5)
    check() shouldBe None

    addFile(6)
    check() shouldBe None

    addFile(7)
    check() shouldBe None

    addFile(3)
    check() shouldBe None

    syncQuery(FilesInStorageTbl.q.result).length shouldBe 6
  }
  it should "return an identical file with same hash found" in withFile { files =>
    AppConfig.storage.configureStorage
    val storedFile = instance.prepareStoredFile(files.head)

    syncQuery(FilesInStorageTbl.q += storedFile)

    val result = Await.result(instance.checkForIdentical(files.head), 20.seconds)
    result.get shouldBe storedFile.copy(id = 1)
  }
  it should "return an identical file with different hash found" in withFile { files =>
    AppConfig.storage.configureStorage
    val storedFile = instance.prepareStoredFile(files.head)

    syncQuery(FilesInStorageTbl.q += storedFile)

    files.head.appendLine("Alter content")
    storedFile.hash should not be instance.prepareStoredFile(files.head).hash

    val result = Await.result(instance.checkForIdentical(files.head), 20.seconds)
    result.get shouldBe storedFile.copy(id = 1)
  }
  "addNewFile" should "add new file to the system" in withFile { files =>
    AppConfig.storage.configureStorage

    val sf = instance.prepareStoredFile(files.head).copy(id = 1)

    val result = runFuture(instance.addNewFile(files.head))

    result shouldBe FileAdded(sf)


    val data = syncQuery(FilesInStorageTbl.q.result)

    data.length shouldBe 1
    data.head shouldBe sf
  }
  "updateHash" should "update hash for identical file" in withFile { files =>
    AppConfig.storage.configureStorage
    val sf = instance.prepareStoredFile(files.head)

    syncQuery(FilesInStorageTbl.q += sf.copy(hash = "000001"))

    val result = runFuture(instance.updateHash(sf))

    result shouldBe FileHashUpdated(sf)
  }
}
