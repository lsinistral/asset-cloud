package net.viacheslav.mm.test.storage.core

import java.nio.file.Paths

import better.files.File
import net.viach.mm.storage.processing.ProcessFile
import net.viach.mm.storage.processing.ProcessFile.{FileAdded, FileHashUpdated, FileNotChanged}
import net.viacheslav.mm.core.{Storage, StoragedFile}
import net.viacheslav.mm.test.core.helpers.TestSpec
import net.viacheslav.mm.test.core.helpers.runFuture
import net.viacheslav.mm.test.storage.WithFiles

import scala.concurrent.{Await, ExecutionContext, Future}

class ProcessFileTests extends TestSpec with WithFiles {

  object instance extends ProcessFile {

    implicit val ec: ExecutionContext = ExecutionContext.global

    def checkForIdentical(file: File): Future[Option[StoragedFile]] =
      Future {
        file.name match {
          case "file.txt" => None
          case "file3.txt" => Some(prepareStoredFile(file).copy(hash = "000001"))
          case _ => Some(prepareStoredFile(file))
        }
      }

    def addNewFile(file: File): Future[ProcessFile.FileActionResult] =
      Future { FileAdded(prepareStoredFile(file)) }

    def updateHash(file: StoragedFile): Future[ProcessFile.FileActionResult] =
      Future { FileHashUpdated(file) }

    def publicProcess(file: File): Future[ProcessFile.FileActionResult] = process(file)
  }

  val configStorage = Storage(2, "ConfigurationTestStorage", "/test-config", "/Users/viacheslav/mm-test")

  "prepareStoredFile" should "produce correct StoragedFile" in withFile { files =>
    val storedFile =
      StoragedFile("file.txt", Paths.get("sub/path"), files.head.size, instance.computeHash(files.head), 2, -1)
    val storedFile2 =
      StoragedFile("file4.txt", Paths.get(""), files(4).size, instance.computeHash(files(4)), 2, -1)


    instance.prepareStoredFile(files.head) shouldBe storedFile

    instance.prepareStoredFile(files(4)) shouldBe storedFile2

    instance.prepareStoredFile(files(5)).hash shouldBe instance.prepareStoredFile(files.head).hash

  }

  "process" should "add new file" in withFile { files =>
    val sf = instance.prepareStoredFile(files.head)

    runFuture(instance.publicProcess(files.head)) shouldBe FileAdded(sf)
  }
  it should "ignore existent file with same content" in withFile { files =>
    val sf = instance.prepareStoredFile(files(1))

    runFuture(instance.publicProcess(files(1))) shouldBe FileNotChanged(sf)
  }
  it should "update hash of existent file"  in withFile { files =>
    val sf = instance.prepareStoredFile(files(3))

    runFuture(instance.publicProcess(files(3))) shouldBe FileHashUpdated(sf)
  }
}
