package net.viacheslav.mm.test.storage.core

import net.viach.mm.storage.configuration.Configuration.Exceptions._
import net.viach.mm.storage.configuration.Configurator
import net.viacheslav.mm.core.{Storage, StoragesTbl}
import net.viacheslav.mm.test.core.helpers.{DatabaseTestSpec, syncQuery}
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.{Failure, Success}

class StorageConfigurationTests extends DatabaseTestSpec {

  /**
    * Storage configured in application.conf
    */
  val configStorage = Storage(2, "ConfigurationTestStorage", "/test-config", "/Users/viacheslav/mm-test")

  def getInstance = new Configurator()

  "Storage configurator" should "parse configuration properly" in {
    getInstance.getStorage shouldBe configStorage
  }

  it should "return storage info if storage exist" in {
    /**
      * Storage existence is based on storage name
      * so if storage with the same name exist in the system
      * it must be returned
      */

    val instance = getInstance
    val storage = Storage(1, "Test storage", "/test", "/path/to/directory")
    syncQuery(StoragesTbl.q += storage)
    syncQuery(StoragesTbl.q += configStorage)

    val future = instance.checkStorage("Test storage")
    val result = Await.result(future, 20.seconds)
    result shouldBe Some(storage)

    //Default storage configured in application.conf
    val future2 = instance.checkStorage()
    val result2 = Await.result(future2, 20.seconds)
    result2 shouldBe Some(configStorage.copy(id = 2))
  }

  it should "return None if storage does not exist" in {
    /**
      * Same reasoning as previous
      */
    val instance = getInstance
    val future = instance.checkStorage("No storage")
    val result = Await.result(future, 20.seconds)
    result shouldBe None

    val storage = Storage(1, "Test storage", "/test", "/path/to/directory")
    syncQuery(StoragesTbl.q += storage)
    val future2 = instance.checkStorage("Wrong storage")
    val result2 = Await.result(future2, 20.seconds)
    result2 shouldBe None

    //Default storage configured in application.conf
    val future3 = instance.checkStorage()
    val result3 = Await.result(future3, 20.seconds)
    result3 shouldBe None
  }

  it should "add new storage to system" in {
    val instance = getInstance
    val storage = Storage(1, "Test storage", "/test", "/path/to/directory")
    val future = instance.addStorage(storage)
    val result = Await.result(future, 20.seconds)

    result shouldBe Success(1)

    val data: Seq[Storage] = syncQuery(StoragesTbl.q.result)

    data.count(_ => true) shouldBe 1

    data.head shouldBe storage
  }

  it should "not add storage with the same name to the system" in {
    val instance = getInstance
    val storage = Storage(1, "Test storage", "/test", "/path/to/directory")
    val storage2 = Storage(2, "Test storage", "/test-dup", "/path/to/duplicate")

    syncQuery(StoragesTbl.q += storage)

    val future = instance.addStorage(storage2)
    val result = Await.result(future, 20.seconds)

    result shouldBe a[Failure[_]]
    result match {
      case Failure(x) =>
        x shouldBe a[DuplicateNameException]
        x.asInstanceOf[DuplicateNameException].name shouldBe "Test storage"
    }
  }

  it should "not add storage with the same system path, as already present in the system" in {
    val instance = getInstance
    val storage = Storage(1, "Test storage", "/test", "/path/to/directory")
    val storage2 = Storage(2, "Test storage same path", "/test", "/path/to/duplicate")

    syncQuery(StoragesTbl.q += storage)

    val future = instance.addStorage(storage2)
    val result = Await.result(future, 20.seconds)

    result shouldBe a[Failure[_]]
    result match {
      case Failure(x) =>
        x shouldBe a[DuplicatePathException]
        x.asInstanceOf[DuplicatePathException].systemPath shouldBe "/test"
    }
  }

  it should "update configuration" in {
    val instance = getInstance
    /**
      * changing systemPath to check how currentStorage will be updated
      * changing localPath to check if it will be changed in the system
      */
    val alteredStorage = configStorage.copy(systemPath = "/altered", localPath = "/subdirectory")

    /**
      * fixing only local path, as system path is taken from the system
      * and local path is considered to be truthful in the configuration file
      */
    val fixedStorage = alteredStorage.copy(localPath = configStorage.localPath)

    syncQuery(StoragesTbl.q += alteredStorage)

    instance.getStorage shouldBe configStorage

    instance.updateConfig(alteredStorage) shouldBe Success(fixedStorage)

    val data: Seq[Storage] = syncQuery(StoragesTbl.q.result)

    data.count(_ => true) shouldBe 1

    //changing ID because updateConfig() assumes passed storage is from the system
    // and do not check and/or change ID field
    data.head shouldBe fixedStorage.copy(id = 1)

    instance.getStorage shouldBe fixedStorage

  }

  it should "configure nonexistent storage" in {
    val instance = getInstance
    val fixedStorage = configStorage.copy(id = 2)
    val otherStorage = Storage(1, "Other storage", "/other", "/path/to/other")

    syncQuery(StoragesTbl.q += otherStorage)

    instance.configureStorage shouldBe Success(fixedStorage)
    instance.getStorage shouldBe fixedStorage

    val data: Seq[Storage] = syncQuery(StoragesTbl.q.result)
    data.count(_ => true) shouldBe 2
    data.filter(_.id == 2).head shouldBe fixedStorage
    }

  it should "configure existing storage with no changes" in {
    val instance = getInstance
    val fixedStorage = configStorage.copy(id = 1)
    syncQuery(StoragesTbl.q += configStorage)

    instance.getStorage shouldBe configStorage
    instance.configureStorage shouldBe Success(fixedStorage)
    instance.getStorage shouldBe fixedStorage
  }

  it should "configure existing storage with changes" in {
    val instance = getInstance
    val alteredStorage = configStorage.copy(systemPath = "/altered", localPath = "/subdirectory")
    val fixedStorage = configStorage.copy(id = 1, systemPath = "/altered")
    syncQuery(StoragesTbl.q += alteredStorage)

    instance.getStorage shouldBe configStorage
    instance.configureStorage shouldBe Success(fixedStorage)
    instance.getStorage shouldBe fixedStorage
  }
}
