import React, { Component } from 'react';
import './App.css';

import MainNav from './components/MainNav';
import StoragesManager from "./components/StoragesManager";
import TagsManager from "./components/TagsManager";
import EntitiesManager from "./components/EntitiesManager"
import {Route, Switch} from "react-router-dom";

class App extends Component {
  render() {
    return (
      <div className="uk-container">
          <div data-uk-grid>
              <MainNav/>
              <Switch>
              <Route path="/storages" component={StoragesManager} />
              <Route path="/tags" component={TagsManager} />
              <Route path="/entities" component={EntitiesManager} />
              </Switch>
          </div>
      </div>
    );
  }
}

export default App;
