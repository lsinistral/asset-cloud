import React, {Component} from 'react';
import {Link} from "react-router-dom";

class MainNav extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [
                {path: "entities", text: "Entities", active: false},
                {path: "storages", text: "Storages", active: false},
                {path: "tags", text: "Tags", active: false},
                {path: "files", text: "Files", active: false}
        ]};

    }
    onItemClick = (id) => (e) => {

    };
    render() {
        const items = this.state.items.map( (item, idx) => {
            return<li key={idx}
                      className={item.active ? "uk-active" : ""}
                      onClick={this.onItemClick(idx)}>
                <Link to={item.path}>{item.text}</Link>
            </li>
            });

        return (
          <nav data-uk-navbar className="uk-width-1-1@m uk-navbar-container">
              <div className="uk-navbar-left">
                  <ul className="uk-navbar-nav">
                      {items}
                  </ul>
              </div>
          </nav>
        );
    }
}


export default MainNav