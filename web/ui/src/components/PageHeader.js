import React, { Component } from 'react';


class PageHeader extends Component {
    render() {
        const text = this.props.text;
        return (
                <h1 className="uk-heading-primary">{text}</h1>
        );
    }
}

export default PageHeader;