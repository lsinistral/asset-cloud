import React, {Component} from 'react'
import {StoragesTableContainer} from "./storagesManager/StoragesTable";
import PageHeader from './PageHeader'

class StoragesManager extends Component {
    updateStorage = (e) => {
        console.log(e)
    };
    render() {
        return (
            <div className="uk-width-1-1@m">
                <PageHeader text="Storages"/>
                <StoragesTableContainer/>
            </div>
        );
    }
}

export default StoragesManager;