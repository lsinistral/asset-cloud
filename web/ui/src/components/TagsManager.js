import React, {Component} from 'react'
import PageHeader from "./PageHeader";
import TagsList from "./tagsManager/TagsList";

class TagsManager extends Component {
    render() {
        return (
            <div id="tagsManager" className="uk-width-1-1@m">
                <PageHeader text="Tags"/>
                <TagsList/>
            </div>
        );
    }
}

export default TagsManager;