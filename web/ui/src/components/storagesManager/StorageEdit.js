import React, {Component} from 'react'

class StorageEdit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id:         this.props.storage.id,
            name:       this.props.storage.name,
            systemPath: this.props.storage.systemPath,
            localPath:  this.props.storage.localPath
        }
}
    saveEvent = (e) => {
        this.props.onEditToggle(e);
        this.props.onSaveClick(this.state)
    };

    onChangeHandler = (e) => {
        e.preventDefault();
        this.setState({ [e.target.id] : e.target.value});
        };

    render() {
        const id = this.props.storage.id;
        const name = this.props.storage.name;
        const system = this.props.storage.systemPath;
        const local = this.props.storage.localPath;

        return (
            <tr onChange={this.onChangeHandler}>
                <td>{id}</td>
                <td><input id="name" type="text" className="uk-input" defaultValue={name}/></td>
                <td><input id="systemPath" type="text" className="uk-input" defaultValue={system}/></td>
                <td><input id="localPath" type="text" className="uk-input" defaultValue={local}/></td>
                <td>
                    <button className="uk-button uk-button-primary"
                            onClick={this.saveEvent}>Save</button>
                </td>
            </tr>
        );
    }
}

export default StorageEdit;