import React, {Component} from 'react'
import StorageShow from "./StorageShow";
import StorageEdit from "./StorageEdit";

class StorageRow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inEdit: false
        };
    }

    toggleEdit = (e) => {
        this.setState({
            inEdit: !this.state.inEdit
        })
    };

    render() {
       if (this.state.inEdit){
           return <StorageEdit onEditToggle={this.toggleEdit}
                               storage={this.props.storage}
                               onSaveClick={this.props.onSaveClick}/>
       }
       else {
           return <StorageShow toggl={this.toggleEdit} storage={this.props.storage}/>
       }
    }
}

export default StorageRow;