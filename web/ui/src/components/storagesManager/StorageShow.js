import React, {Component} from 'react'

class StorageShow extends Component {
    render() {
        const id = this.props.storage.id;
        const name = this.props.storage.name;
        const system = this.props.storage.systemPath;
        const local = this.props.storage.localPath;
        return (
            <tr className="uk-visible-toggle">
                <td>{id}</td>
                <td>{name}</td>
                <td><code>{system}</code></td>
                <td><code>{local}</code></td>
                <td>
                    <button className=
                                "uk-button
                                uk-button-default
                                uk-invisible-hover"
                    onClick={this.props.toggl}>Edit</button>
                </td>
            </tr>
        );
    }
}

export default StorageShow;