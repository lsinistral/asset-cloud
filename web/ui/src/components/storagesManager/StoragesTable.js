import React, {Component} from 'react'
import StorageRow from "./StorageRow";
import {connect} from 'react-redux'
import {updateStorage} from "../../data/actions";

export class StoragesTable extends Component {
    render() {
        const rows = [];

        this.props.storages.forEach((storage) => {
            rows.push(<StorageRow key={storage.id}
                                  storage={storage}
                                  onSaveClick={this.props.onSaveClick}/>
            )
        });


        return (
            <table className="uk-table">
                <thead>
                <tr>
                    <th className="uk-table-shrink">ID</th>
                    <th className="uk-width-1-5@m">Name</th>
                    <th className="uk-width-1-5@m">System path</th>
                    <th className="uk-width-1-5@m">Storage local path</th>
                    <th className="uk-width-1-5@m"></th>
                </tr>
                </thead>
                <tbody>
                {rows}
                </tbody>
            </table>


        );
    }
}

const mapStateToProps = (state) => {
    return {
        storages: state.storages
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onSaveClick: (storage) => {
            dispatch(updateStorage(storage))
        }
    }
};
export const StoragesTableContainer =
    connect(mapStateToProps, mapDispatchToProps)(StoragesTable);
// export default StoragesTable;