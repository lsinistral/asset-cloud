export const STORAGE_ADD = 'STORAGE_ADD';
export const STORAGE_UPDATE = 'STORAGE_UPDATE';

export const addStorage = (storage) => {
    return {
        type: STORAGE_ADD,
        storage: storage
    }
};

export const updateStorage = (storage) => {
    return {
        type: STORAGE_UPDATE,
        storage: storage
    }
};


export const TAG_ADD = "TAG_ADD";
export const TAG_DELETE = 'TAG_DELETE';
export const TAG_ADD_TYPE = 'TAG_ADD_TYPE';

export const addTag = (tag) => {
  return {
      type: TAG_ADD,
      tag: tag
  }
};