import {combineReducers} from 'redux'
import storages from './reducers/storages'
import tags from './reducers/tags'

export default combineReducers({
    storages,
    tags
});

