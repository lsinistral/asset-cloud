import {STORAGE_ADD, STORAGE_UPDATE} from "../actions";

export default (state = [], action) => {
    switch (action.type) {
        case STORAGE_ADD:
        case STORAGE_UPDATE: {
            const temp = state.slice();
            temp[action.storage.id] = action.storage;
            return temp
        }
        default:
            return state
    }
};