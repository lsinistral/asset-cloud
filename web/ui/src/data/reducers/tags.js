import {TAG_ADD, TAG_ADD_TYPE, TAG_DELETE} from "../actions";

export default (state = [], action) => {
    switch (action.type) {
        case TAG_ADD:
        case TAG_DELETE:
        case TAG_ADD_TYPE:
            return state;
        default:
            return state
    }
};