import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import "./fonts.css"
import registerServiceWorker from './registerServiceWorker';
import UIkit from 'uikit/dist/js/uikit';
import Icons from 'uikit/dist/js/uikit-icons';
import reducer from './data/reducer'
import {createStore} from 'redux';
import {addStorage} from './data/actions'
import {Provider} from 'react-redux'
import {BrowserRouter} from 'react-router-dom'


const store = createStore(reducer);

store.dispatch(
    addStorage({id: 1, name: "Test", systemPath: "/test", localPath: "/users"})
);

store.dispatch(
    addStorage({id: 2, name: "Second", systemPath: "/test2", localPath: "/mnt/share"})
);
store.dispatch(
    addStorage({id: 3, name: "Third", systemPath: "/test3", localPath:"/mnt/onedrive"})
);
ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
    <App />
        </BrowserRouter>
    </Provider>
    , document.getElementById('root'));
registerServiceWorker();
